from django.db import models

class TodoItems(models.Model):
    items = models.CharField(max_length=120, null=False, blank=False)

    objects = models.Manager()

