from .models import TodoItems
from django.views.generic import ListView
from django.views.generic.edit import DeleteView
from django.shortcuts import get_object_or_404, redirect
from django.http import JsonResponse

class TodoList(ListView):
    print("ppppp")
    model = TodoItems
    template_name = "items.html"

def deleteList(request,id):

    if request.is_ajax():
        print('deleting')
        TodoItems.objects.filter(id=id).delete()
        
        data = {
            'success': 'success'
        }

        return JsonResponse(data)
        

    return redirect('items:list')