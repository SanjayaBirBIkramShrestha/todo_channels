from django.urls import path, include
from .views import TodoList, deleteList

app_name = "items"

urlpatterns = [
    path('',TodoList.as_view(), name="list" ),
    path('delete/<int:id>/',deleteList, name="delete" ),
]