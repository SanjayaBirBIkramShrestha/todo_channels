import json
from django.core.serializers import serialize
from channels.generic.websocket import WebsocketConsumer
from .models import TodoItems
import asyncio

class ItemListConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        item = text_data_json['todo_item']
        TodoItems.objects.create(items=item)
        items = TodoItems.objects.all()
        data = json.loads(serialize('json', items))
        self.send(text_data=json.dumps({
            'todo_list': data
        }))